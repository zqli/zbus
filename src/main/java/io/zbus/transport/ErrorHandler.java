package io.zbus.transport;

public interface ErrorHandler { 
	void handle(Throwable e);   
}